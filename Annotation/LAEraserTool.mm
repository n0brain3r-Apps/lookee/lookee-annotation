//
//  LEraser.m
//  Writeability
//
//  Created by Ryan on 9/15/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "LAEraserTool.h"

#import <Rendering/LGLUtilities.h>

#import <vector>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LAEraserTool
#pragma mark -
//*********************************************************************************************************************//




@implementation LAEraserTool

#pragma mark - Instance Variables
//*********************************************************************************************************************//
{
    std::vector<GLfloat> _vertices;
}


#pragma mark - LAProtocol
//*********************************************************************************************************************//

- (void)initializeTool
{
}


- (GLvoid)beginAtVector:(LGLVector2)vector
{
    _vertices.clear();
}

- (GLvoid)moveToVector:(LGLVector2)vector
{
    _vertices.clear();

    _vertices.push_back(vector.x);
    _vertices.push_back(vector.y);

    [self setVertexData:WrapSTLVector(_vertices)];
}

- (GLvoid)endAtVector:(LGLVector2)vector
{
    [self moveToVector:vector];
}

@end
