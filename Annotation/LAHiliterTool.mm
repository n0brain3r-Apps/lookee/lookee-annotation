//
//  LHiliter.m
//  Writeability
//
//  Created by Ryan on 9/15/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "LAHiliterTool.h"

#import "LAWeightedLine.h"

#import <Utilities/LGeometry.h>

#import <vector>



//*********************************************************************************************************************//
#pragma mark -
#pragma mark LAHiliterTool
#pragma mark -
//*********************************************************************************************************************//



@interface LAHiliterTool ()

@property (nonatomic, readwrite , assign) LGLVector2 head;

@end


@implementation LAHiliterTool

#pragma mark - Instance Variables
//*********************************************************************************************************************//
{
    std::vector<GLfloat> _vertices;
}


#pragma mark - LAProtocol
//*********************************************************************************************************************//

- (void)initializeTool
{
}


- (GLvoid)beginAtVector:(LGLVector2)vector
{
    _vertices.clear();

    _head = vector;
}

- (GLvoid)moveToVector:(LGLVector2)vector
{
    _vertices.clear();

	GLfloat weight = [self.properties scaledWeight];

	LAWeightedLineSegment(_head,
						 vector,
						 weight,
						 weight,
						 &_vertices,
						 NULL,
						 NULL);

    [self setVertexData:WrapSTLVector(_vertices)];
}

- (GLvoid)endAtVector:(LGLVector2)vector
{
    [self moveToVector:vector];
}

@end
