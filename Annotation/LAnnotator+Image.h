//
//  LAnnotator+Image.h
//  Writeability
//
//  Created by Ryan on 12/26/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "LAnnotator.h"


@interface LAnnotator (Image)

- (void)setImage:(UIImage *)image atPoint:(CGPoint)point;
- (void)setImage:(UIImage *)image inRect:(CGRect)rect;

- (NSUInteger)imageAnnotationIndexForPoint:(CGPoint)point;

@end
