//
//  LAnnotator+Image.m
//  Writeability
//
//  Created by Ryan on 12/26/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "LAnnotator+Image.h"
#import "LAnnotator_Private.h"


#import <Rendering/LGLQuad.h>
#import <Rendering/LGLTexture.h>

#import <Utilities/NSObject+Utilities.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LAnnotator+Image
#pragma mark -
//*********************************************************************************************************************//




@implementation LAnnotator (Image)

#pragma mark - Static Objects
//*********************************************************************************************************************//

static const LAnnotationType kLAnnotationTypeImage = (^(NSArray *actions) {
    for (NSInvocation *action in actions) {
        if ([action.target isKindOfClass:[LGLQuad class]]) {
            return YES;
        }
    }

    return NO;
});

#pragma mark - Public Methods
//*********************************************************************************************************************//

- (void)setImage:(UIImage *)image atPoint:(CGPoint)point
{
    [self setImage:image inRect:((CGRect) { point, image.size })];
}

- (void)setImage:(UIImage *)image inRect:(CGRect)rect
{
    if ([image isKindOfClass:[UIImage class]] && !CGSizeIsZero(image.size) && !CGRectIsEmpty(rect)) {
        CGRect              integral    = CGRectIntegral(rect);
        LGLMatrix4x2        vertices    = LGLRectVertices(integral);

        LATool     *tool       = [LATool newBoxer];
        NSData              *data       = Datafy(vertices);

        LGLTexture          *texture    = [self textureForImage:image];
        
        LGLQuad             *renderer   = [[LGLQuad alloc] initWithTexture:texture];
        NSInvocation        *action     = [renderer retainingInvocationForSelector:@selector(render)];

        [renderer setTarget:integral];

        [self createAnnotationWithTool:tool];
        [self addAction:action toAnnotation:self.lastAnnotation];

        [tool setVertexData:data];
    }
}

- (NSUInteger)imageAnnotationIndexForPoint:(CGPoint)point
{
    return [self indexOfAnnotationWithType:kLAnnotationTypeImage atPoint:point];
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (LGLTexture *)textureForImage:(UIImage *)image
{
    LGLTexture *texture = [[LGLTexture alloc]
                           initWithWidth:image.size.width
                           height       :image.size.height
                           scale        :[LGL scale]];

    [texture clear];
    [texture drawBitmap:^{[image drawAtPoint:CGPointZero];}];

    return texture;
}

@end
