//
//  LAnnotator+Text.h
//  Writeability
//
//  Created by Ryan on 12/26/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "LAnnotator.h"


@interface LAnnotator (Text)

- (void)setText:(NSAttributedString *)text atPoint:(CGPoint)point;
- (void)setText:(NSAttributedString *)text inRect:(CGRect)rect;

- (NSUInteger)textAnnotationIndexForPoint:(CGPoint)point;

@end
