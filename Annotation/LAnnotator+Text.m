//
//  LAnnotator+Text.m
//  Writeability
//
//  Created by Ryan on 12/26/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "LAnnotator+Text.h"
#import "LAnnotator_Private.h"

#import <Rendering/LGLText.h>

#import <Utilities/NSObject+Utilities.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LAnnotator+Text
#pragma mark -
//*********************************************************************************************************************//




@implementation LAnnotator (Text)

#pragma mark - Static Objects
//*********************************************************************************************************************//

static const LAnnotationType kLAnnotationTypeText = (^(NSArray *actions) {
    for (NSInvocation *action in actions) {
        if ([action.target isKindOfClass:[LGLText class]]) {
            return YES;
        }
    }

    return NO;
});


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (void)setText:(NSAttributedString *)text atPoint:(CGPoint)point
{
    [self setText:text inRect:((CGRect){point, text.size})];
}

- (void)setText:(NSAttributedString *)text inRect:(CGRect)rect
{
    if ([text isKindOfClass:[NSAttributedString class]] && [text length] && !CGRectIsEmpty(rect)) {
        CGRect              integral    = CGRectIntegral(rect);
        LGLMatrix4x2        vertices    = LGLRectVertices(integral);

        LATool     *tool       = [LATool newBoxer];
        NSData              *data       = Datafy(vertices);

        LGLText             *renderer   = [[LGLText alloc] initWithFrame:integral];
        NSInvocation        *action     = [renderer retainingInvocationForSelector:@selector(render)];

        [renderer setAttributedString:text];

        [self createAnnotationWithTool:tool];
        [self addAction:action toAnnotation:self.lastAnnotation];

        [tool setVertexData:data];
    }
}

- (NSUInteger)textAnnotationIndexForPoint:(CGPoint)point
{
    NSUInteger index = [self indexOfAnnotationWithType:kLAnnotationTypeText atPoint:point];

    return index;
}

@end
